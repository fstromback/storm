#pragma once
#include "Container.h"

namespace gui {

	/**
	 * Box that groups items that logically belongs together.
	 */
	class Group : public SingleChildContainer {
		STORM_CLASS;
	public:
		// Create a group without a label.
		STORM_CTOR Group();

		// Create and specify the label.
		STORM_CTOR Group(Str *title);

		// Set the text.
		virtual void STORM_ASSIGN text(Str *str);
		using SingleChildContainer::text;

		// Get minimum size.
		virtual Size STORM_FN minSize();

		// Detect when we are resized.
		virtual void STORM_FN resized(Size size);

	protected:
		// Called to create the control.
		virtual bool create(ContainerBase *parent, nat id);

	private:
#ifdef GUI_WIN32
		// Update child position.
		void updateChildPos();
#endif

		// Remember the height of the text.
		Float textHeight;
	};

}
