#include "stdafx.h"
#include "Separator.h"
#include "Container.h"

namespace gui {

	HSeparator::HSeparator() {}

	HSeparator::HSeparator(Point pos, Float width) {
		this->pos(Rect(pos, Size(width)));
	}

#ifdef GUI_WIN32

	void HSeparator::pos(Rect pos) {
		// + 2 is from what Explorer uses. We do DPI scaling after this, so it should be fine.
		Window::pos(Rect(pos.p0.x, pos.p0.y, pos.p1.x, pos.p0.y + 2));
	}

	bool HSeparator::create(ContainerBase *parent, nat id) {
		return createEx(WC_STATIC, childFlags | SS_ETCHEDHORZ, 0, parent->handle().hwnd(), id);
	}

#endif
#ifdef GUI_GTK

	void HSeparator::pos(Rect pos) {
		Window::pos(pos);
	}

	bool HSeparator::create(ContainerBase *parent, nat id) {
		initWidget(parent, gtk_separator_new(GTK_ORIENTATION_HORIZONTAL));
		return false;
	}

#endif


	VSeparator::VSeparator() {}

	VSeparator::VSeparator(Point pos, Float width) {
		this->pos(Rect(pos, Size(width)));
	}

#ifdef GUI_WIN32

	void VSeparator::pos(Rect pos) {
		// + 2 is from what Explorer uses. We do DPI scaling after this, so it should be fine.
		Window::pos(Rect(pos.p0.x, pos.p0.y, pos.p1.x + 2, pos.p1.y));
	}

	bool VSeparator::create(ContainerBase *parent, nat id) {
		return createEx(WC_STATIC, childFlags | SS_ETCHEDVERT, 0, parent->handle().hwnd(), id);
	}

#endif
#ifdef GUI_GTK

	void VSeparator::pos(Rect pos) {
		Window::pos(pos);
	}

	bool VSeparator::create(ContainerBase *parent, nat id) {
		initWidget(parent, gtk_separator_new(GTK_ORIENTATION_VERTICAL));
		return true;
	}

#endif


}
