#include "stdafx.h"
#include "Message.h"
#include "MsgName.h"

#ifdef GUI_WIN32

namespace gui {

	Message::Message(const MSG &src) : msg(src.message), wParam(src.wParam), lParam(src.lParam) {}

	Message::Message(UINT msg, WPARAM wParam, LPARAM lParam) :
		msg(msg), wParam(wParam), lParam(lParam) {}

	void Message::output(wostream &to) const {
		to << msgName(msg) << L" (" << msg << L"), " << wParam << L", " << lParam;
	}

	MsgResult noResult() {
		MsgResult r = { false, false, 0 };
		return r;
	}

	MsgResult noResultNotify() {
		MsgResult r = { false, true, 0 };
		return r;
	}

	MsgResult msgResult(LRESULT code) {
		MsgResult r = { true, false, code };
		return r;
	}

}

#endif
