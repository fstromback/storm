#pragma once
#include "Window.h"

namespace gui {

	/**
	 * Simple horizontal separator.
	 */
	class HSeparator : public Window {
		STORM_CLASS;
	public:
		// Create.
		STORM_CTOR HSeparator();
		STORM_CTOR HSeparator(Point pos, Float width);

		// Set position.
		virtual void STORM_ASSIGN pos(Rect pos);

	protected:
		virtual bool create(ContainerBase *parent, nat id);
	};


	/**
	 * Simple vertical separator.
	 */
	class VSeparator : public Window {
		STORM_CLASS;
	public:
		// Create.
		STORM_CTOR VSeparator();
		STORM_CTOR VSeparator(Point pos, Float height);

		// Set position.
		virtual void STORM_ASSIGN pos(Rect pos);

	protected:
		virtual bool create(ContainerBase *parent, nat id);
	};

}
