use progvis;
use test;
use lang:bs:macro;
use core:io;

private Url[] findAllFiles(Url path) {
	Url[] result;
	for (child in path.children) {
		if (child.dir) {
			result.append(findAllFiles(child));
		} else if (!child.title.endsWith("_error")) {
			result << child;
		}
	}
	result;
}

test CompileC {
	unless (url = examplesDir())
		abort;

	// TODO: Should we compile *all* examples? That is way more expensive...
	for (file in findAllFiles(url / "test")) {
		check program:Program:load(file) no throw;
		// TODO: We could also try to run the program, just to make sure that we don't crash instantly.
	}
}
