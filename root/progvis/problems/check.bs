use core:io;
use progvis:net;
use progvis:check;

// Wrapper over the 'check' library. Accounts for the `ProblemChecks` type from `net`.
Result? check(ProblemChecks checks, Url[] files) {
	progvis:check:check(files, convert(checks));
}

Result? check(ProblemChecks checks, Url[] files, Progress callback) {
	progvis:check:check(files, callback, convert(checks));
}

Checks convert(ProblemChecks checks) {
	progvis:check:Checks out;
	out.termination = checks.termination;
	out.leaks = checks.leaks;
	return out;
}
