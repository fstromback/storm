use core:lang;
use core:asm;

/**
 * Ability to mark allocations as "leakable" (i.e. they need to be tracked with extra care). Exports
 * this information as a "pseudo-instruction" that can be used to mark leaks. This instruction will
 * expand to a function call, so should be considered to clobber registers, even if that may not
 * always happen.
 */

class LeakableAlloc {
	init(Operand alloc) {
		init { alloc = alloc; }
	}

	Operand alloc;

	void toS(StrBuf to) : override {
		to << "Leakable allocation: " << alloc;
	}
}
