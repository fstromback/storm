use util:serialize;
use core:io;
use parser;

/**
 * Misc. data types.
 */


/**
 * Representation of a single problem (no source code, intended for lists of problems).
 */
class ProblemInfo : serializable {
	// Problem ID.
	Int id;

	// Problem title.
	Str title;

	// Problem author.
	Str author;

	// Is the next step to modify the implementation?
	Bool hasError;

	// Does the current user have any solutions to this problem?
	Bool attempted;

	// Create.
	init(Int id, Str title, Str author, Bool hasError, Bool attempted) {
		init {
			id = id;
			title = title;
			author = author;
			hasError = hasError;
			attempted = attempted;
		}
	}

	// For sorting.
	Bool <(ProblemInfo o) {
		id < o.id;
	}

	// To string.
	void toS(StrBuf to) : override {
		to << "{id: " << id << ", title: " << title << ", author: " << author << ", hasError: " << hasError << ", attempted: " << attempted << "}";
	}
}


/**
 * Corresponds to `progvis:check:Checks`, but customized for the options relevant to the online
 * system. Also here to avoid the network server to depend on the UI library.
 */
class ProblemChecks : serializable {
	// Check that the program terminates.
	Bool termination;

	// Check that the program does not leak memory.
	Bool leaks;

	// Create.
	init(Bool termination, Bool leaks) {
		init {
			termination = termination;
			leaks = leaks;
		}
	}

	// To string.
	void toS(StrBuf to) : override {
		to << "{termination: " << termination << ", leaks: " << leaks << "}";
	}

	// Create a DB representation.
	Str toDB() {
		StrBuf out;
		if (termination)
			out << "t";
		if (leaks)
			out << "l";
		return out.toS();
	}

	// Parse DB representation.
	ProblemChecks fromDB(Str repr) : static {
		ProblemChecks out(false, false);

		for (x in repr) {
			if (x == 't')
				out.termination = true;
			else if (x == 'l')
				out.leaks = true;
		}

		return out;
	}
}


/**
 * Errors that are acceptable on the initial submission of a problem.
 *
 * Similar to ProblemChecks above, but might be extended in different dimensions later. Note that
 * the errors here should be somewhat "deterministic" from the model checker - that is, we want to
 * make sure that the acceptable errors do not "hide" any other type of error (e.g. a deadlock
 * hiding a real concurrency issue).
 */
class AcceptableErrors : serializable {
	// Check that the program terminates.
	Bool termination;

	// Check that the program does not leak memory.
	Bool leaks;

	// Create.
	init(Bool termination, Bool leaks) {
		init {
			termination = termination;
			leaks = leaks;
		}
	}

	// To string.
	void toS(StrBuf to) : override {
		to << "{termination: " << termination << ", leaks: " << leaks << "}";
	}

	// Create a DB representation.
	Str toDB() {
		StrBuf out;
		if (termination)
			out << "t";
		if (leaks)
			out << "l";
		return out.toS();
	}

	// Parse DB representation.
	AcceptableErrors fromDB(Str repr) : static {
		AcceptableErrors out(false, false);

		for (x in repr) {
			if (x == 't')
				out.termination = true;
			else if (x == 'l')
				out.leaks = true;
		}

		return out;
	}

	// Check if an error is acceptable.
	Bool check(Str type) {
		if (termination)
			if (type == "livelock")
				return true;
		if (leaks)
			if (type == "memory leak")
				return true;
		return false;
	}
}


/**
 * Representation of something that we can attempt to solve. Includes the code necessary for the
 * client to solve the problem. Also contains information about the origins of the source code so
 * that a solution can be submitted.
 */
class Problem : serializable {
	// ID of the problem we are solving.
	Int id;

	// Title of the problem.
	Str title;

	// If positive: ID of the implementation we are using.
	Int implId;

	// Version of the implementation.
	Int implVersion;

	// If positive: ID of the test we are using.
	Int testId;

	// Version of the test.
	Int testVersion;

	// Does the implementation + test expose an error?
	Error error;

	// Implementation.
	Code impl;

	// Test program.
	Code test;

	// Reference implementation.
	Code ref;

	// Enabled checks.
	ProblemChecks checks;

	// Acceptable errors.
	AcceptableErrors acceptableErrors;

	// Create.
	init(Int id, Str title, Int implId, Int implVer, Int testId, Int testVer, Error error, Code impl, Code test, Code ref, ProblemChecks checks, AcceptableErrors acceptable) {
		init {
			id = id;
			title = title;
			implId = implId;
			implVersion = implVer;
			testId = testId;
			testVersion = testVer;
			error = error;
			impl = impl;
			test = test;
			ref = ref;
			checks = checks;
			acceptableErrors = acceptable;
		}
	}
}


/**
 * A piece of code, indicating a language as well.
 */
class Code : serializable {
	// ID in the database.
	Int id;

	// Source code.
	Str src;

	// Language (= file extension).
	Str language;

	// Create.
	init(Str src, Str language) {
		init {
			id = -1;
			src = src;
			language = language;
		}
	}

	// Create.
	init(Int id, Str src, Str language) {
		init {
			id = id;
			src = src;
			language = language;
		}
	}

	// Add to a MemoryProtocol.
	Url put(Str base, MemoryProtocol to) {
		MemOStream out;
		Utf8Output text(out);
		text.write(src);
		text.flush();
		to.put(base + "." + language, out.buffer);
	}

	// Produce a "signature" for this code.
	Str signature() {
		codeSignature(src);
	}
}

/**
 * Type of improvement: test or implementation.
 */
enum ImprovementType {
	test,
	implementation,
}


/**
 * Improvement to a problem. Either a test or an implementation, depending on context.
 */
class Improvement : serializable {
	// ID of this improvement.
	Int id;

	// Version of the improvement.
	Int version;

	// Author.
	Str author;

	// Error?
	Error error;

	// Version that found the error.
	Int errorVersion;

	// Create.
	init(Int id, Int version, Str author, Error error) {
		init {
			id = id;
			version = version;
			author = author;
			error = error;
		}
	}
}

/**
 * Error status of a problem.
 * Represents what type of error a problem has, and if the status is unknown.
 */
value Error : serializable {
	// Error message, if any. Empty string means that all is well. Null means unknown.
	private Str? data;

	// Internal helper.
	private init(Str? data) {
		init { data = data; }
	}

	Error error(Str type) : static { Error(type); }
	Error success() : static { Error(""); }
	Error unknown() : static { Error(null); }

	// Is it a success?
	Bool success() {
		if (data)
			return data.empty;
		false;
	}

	// Is it an error?
	Str? error() {
		if (data)
			if (data.any)
				return data;
		return null;
	}

	// Is it unknown?
	Bool unknown() {
		data.empty;
	}

	// Do we have an error?
	Bool any() {
		error.any;
	}

	Str toS() {
		if (data) {
			if (data.empty) {
				return "No error";
			} else {
				return "Error: " + data;
			}
		} else {
			return "Unknown";
		}
	}
}

/**
 * A single high-score entry.
 */
class Score : serializable {
	Str name;
	Int points;
	Int place;

	init(Str name, Int points, Int place) {
		init {
			name = name;
			points = points;
			place = place;
		}
	}
}


/**
 * A triplet of problem + implementation + test that uniquely identifies some combination that can
 * be tested.
 */
value CodeTriplet : serializable {
	Int problem;
	Int implementation;
	Int test;

	init(Int problem, Int implementation, Int test) {
		init {
			problem = problem;
			implementation = implementation;
			test = test;
		}
	}

	void toS(StrBuf to) {
		to << "{problem: " << problem << ", impl: " << implementation << ", test: " << test << "}";
	}
}

// Create a "signature" that allows comparing code for equality.
Str codeSignature(Str code) {
	sigParser(code).value.toS;
}

// Remove whitespace from a string to create a "hash" of a program. Note: This is not 100%
// accurate, but good enough to detect "significant" changes (for example "myclass a" and "my
// classa" will hash to the same thing.
sigParser : parser(recursive descent) {
	start = Start;

	StrBuf Start();
	Start => StrBuf() : "[ \n\r\t]*" - ("[^ \n\r\t]+" -> add - "[ \n\r\t]*")*;
}
