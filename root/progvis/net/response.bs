use core:io;
use util:serialize;
use core:info;
use lang:bs:macro;

/**
 * This is the base class for all responses from the server to the client.
 */
class Response : serializable {}

/**
 * Error response. Contains some kind of error message.
 */
class ErrorResponse : extends Response, serializable {
	Str message;

	init(Str message) {
		init { message = message; }
	}
}

/**
 * Authentication response. Successful case.
 */
class AuthResponse : extends Response, serializable {
	// User name.
	Str name;

	// Is the user an admin?
	Bool admin = false;

	// Server version.
	Version version;

	// TODO: Other stats?

	init(Str name, Bool admin) {
		init {
			name = name;
			admin = admin;
			version = named{PROGVIS_VERSION}.version;
		}
	}
}

/**
 * Authentication response. The user must sign in separately.
 */
class AuthLoginResponse : extends Response, serializable {
	// URL to redirect the user to.
	Url url;

	init(Url url) {
		init { url = url; }
	}
}

/**
 * Reply for the logout request. No data.
 */
class LogoutResponse : extends Response, serializable {}

/**
 * Reply for the leaderboard request.
 */
class LeaderboardResponse : extends Response, serializable {
	Score[] scores;

	init(Score[] scores) {
		init { scores = scores; }
	}
}

/**
 * Reply to the problem list query.
 */
class ProblemListResponse : extends Response, serializable {
	ProblemInfo[] data;
	init(ProblemInfo[] data) {
		init { data = data; }
	}
}

/**
 * Response with an individual problem.
 */
class ProblemResponse : extends Response, serializable {
	Problem problem;
	init(Problem problem) {
		init { problem = problem; }
	}
}

/**
 * Response for CurrentStateRequest.
 */
class CurrentStateResponse : extends Response, serializable {
	// Problem id.
	Int id;

	// Problem title.
	Str title;

	// If positive: ID of the latest implementation.
	Int implId;

	// Version of the implementation.
	Int implVersion;

	// Implementation.
	Code impl;

	// IDs for all tests.
	Int[] testIds;

	// Array of available tests. The first one is the original, remaining ones are versions.
	Code[] tests;

	// Error information for tests.
	Error[] errors;

	// Reference implementation.
	Code ref;

	// Enabled checks.
	ProblemChecks checks;

	// Acceptable errors.
	AcceptableErrors acceptableErrors;

	init(Int id, Str title, Int implId, Int implVersion, Code impl,
		Int[] testIds, Code[] tests, Error[] errors,
		Code ref, ProblemChecks checks, AcceptableErrors accErrors) {
		init {
			id = id;
			implId = implId;
			implVersion = implVersion;
			impl = impl;
			testIds = testIds;
			tests = tests;
			errors = errors;
			ref = ref;
			checks = checks;
			acceptableErrors = accErrors;
		}
	}

	Problem problemFor(Nat id) {
		Problem(this.id, title,
				implId, implVersion,
				testIds[id], id.int, errors[id],
				impl, tests[id], ref,
				checks, acceptableErrors);
	}
}


/**
 * Response for submitting a new item in the database.
 */
class NewItemResponse : extends Response, serializable {
	Int id;

	init(Int id) {
		init { id = id; }
	}
}

/**
 * List of available tests or available implementations.
 */
class ImprovementsResponse : extends Response, serializable {
	Int problemId;

	Improvement[] data;

	init(Int id, Improvement[] data) {
		init {
			problemId = id;
			data = data;
		}
	}
}

/**
 * A piece of code.
 */
class CodeResponse : extends Response, serializable {
	Code code;

	init(Code code) {
		init { code = code; }
	}
}

/**
 * A list of things that need to be checked.
 */
class MissingChecksResponse : extends Response, serializable {
	CodeTriplet[] missing;

	init(CodeTriplet[] missing) {
		init { missing = missing; }
	}
}
