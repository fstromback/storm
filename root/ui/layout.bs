use layout;
use core:geometry;

/**
 * Wrappers for using the layout library together with the Ui library.
 */


/**
 * Wrapper around windows in the UI class.
 */
class UiComponent extends Component {
	// The window we're wrapping.
	Window window;

	// Minimum width of the component.
	Float minWidth;

	// Minimum height of the component.
	Float minHeight;

	// Create.
	init(Window window) {
		init() { window = window; }
	}

	// Get position.
	Rect pos() {
		window.pos;
	}

	// Set position.
	assign pos(Rect p) {
		window.pos = p;
	}

	// Minimum size.
	Size minSize() {
		Size r = window.minSize();
		r.w = max(r.w, minWidth);
		r.h = max(r.h, minHeight);
		r;
	}
}

// Create components.
UiComponent component(Window window) {
	UiComponent(window);
}


/**
 * Custom subclass of Container for sub-layouts. This is approximately what "container X" would
 * generate.
 */
class UiContainer extends Container {
	init(Layout layout) {
		init {
			layout = layout;
		}
	}

	Layout layout;

	Size minSize() : override {
		layout.minSize;
	}

	void resized(Size size) : override {
		layout.pos = Rect(size);
	}
}
