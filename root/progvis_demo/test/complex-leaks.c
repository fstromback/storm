struct node {
	struct node *next;
	int value;
};

struct node *head;

void list_add(int value) {
	for ( ; ; ) {
		struct node *h = atomic_read(&head);
		struct node *to_insert = malloc(sizeof(struct node));
		to_insert->next = h;
		to_insert->value = value;
		if (compare_and_swap(&head, h, to_insert) == h)
			break;
	}
}

int main(void) {
	thread_new(&list_add, 20);
	list_add(10);
	return 0;
}
