// Generate different end-states:
int diff;

void thread_fn() {
	int *x = malloc(sizeof(int));
	atomic_write(&diff, 2);
}

int main(void) {
	thread_new(&thread_fn);
	atomic_write(&diff, 1);
	return 0;
}
