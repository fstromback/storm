use ui;
use geometry;

class GroupTest extends Frame {
	Group group;
	Button inside;

	HSeparator sep;

	Group inTab;
	TabView tabs;

	init() {
		init("Group test", Size(400, 400)) {
			group("Group");
			inside("&Inside");
			inTab("Tab");
		}

		group.pos = Rect(8, 8, 392, 200);

		add(group);
		Container gc;
		group.content = gc;
		gc.add(inside);

		inside.pos = Rect(0, 0, 90, 90);

		sep.pos = Rect(20, 100, 392, 0);
		gc.add(sep);

		add(tabs);
		tabs.add("Tab 1", inTab);

		Container w;
		w.add(Group("Z"));
		CheckButton c("Check");
		c.pos = Rect(100, 30, 200, 50);
		w.add(c);
		tabs.add("Tab 2", w);

		create();
	}

	void resized(Size size) {
		Rect pos = Rect(Point(), size).shrink(Size(8, 8));
		Rect gPos = pos;
		gPos.p1.y = gPos.center.y - 4;
		group.pos = gPos;

		Rect tPos = pos;
		tPos.p0.y = tPos.center.y + 4;
		tabs.pos = tPos;
	}
}

void groups() {
	GroupTest().waitForClose;
}
