use ui;
use layout;
use core:geometry;

frame NestedWindow {
	layout Grid {
		wrapCols: 1;
		expandCol: 0;
		expandRow: 0;
		expandRow: 1;

		Group("Group") {
			content: container Grid {
				expandCol: 0;
				Button a("A") {}
			};
		}

		TabView {
			tab: "Tab 1", container Grid {
				Button b("B") {}
			} /* note: special case without semicolon */
			tab: "Tab 2", container Grid {
				Button c("C") {}
			};
		}

		Button last("Last") {}
	}

	init() {
		init("Nested layout", Size(400, 400));

		create();
	}
}

void nested() {
	NestedWindow().waitForClose;
}
