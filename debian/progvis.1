.\"                                      Hey, EMACS: -*- nroff -*-
.\" (C) Copyright 2021 Filip Strömbäck <filip@fprg.se>,
.\"
.\" First parameter, NAME, should be all caps
.\" Second parameter, SECTION, should be 1-8, maybe w/ subsection
.\" other parameters are allowed: see man(7), man(1)
.TH PROGVIS 1 "October 15 2023"
.\" Please adjust this date whenever revising the manpage.
.\"
.\" Some roff macros, for reference:
.\" .nh        disable hyphenation
.\" .hy        enable hyphenation
.\" .ad l      left justify
.\" .ad b      justify to both left and right margins
.\" .nf        disable filling
.\" .fi        enable filling
.\" .br        insert line break
.\" .sp <n>    insert n+1 empty lines
.\" .RS        indent, for code examples
.\" .RE        decrease relative indent
.\" for manpage-specific macros, see man(7)
.SH NAME
progvis - visualization tool for concurrent C/C++ programs
.SH SYNOPSIS
.B progvis
.SH DESCRIPTION
Progvis is a program visualization tool aimed at concurrent C/C++
programs. It allows loading arbitrary programs and stepping through
them. The tool also informs about a number of concurrency issues, such
as race conditions. Only a subset of C/C++ is supported. For C, most
notably \fIvoid\fR pointers are not supported (for type safety). For
C++, only fundamental language constructs are implemented (e.g. no
templates). Much of the standard libraries both for C and C++ are not
implemented either.
.SH SYNCHRONIZATION
The system provides C-style synchronization primitives as implemented in Pintos, see:
.UR http://www.scs.stanford.edu/07au-cs140/pintos/pintos_6.html#SEC97
.UE
In summary, they are as follows:
.TP
.B struct semaphore
An implementation of a semaphore.
.TP
.B sema_init(struct semaphore *sema, int value)
Initialize a semaphore object to a particular (positive) value.
.TP
.B sema_up(struct semaphore *sema)
Increase the counter in the semaphore. Also known as \fIsignal\fR.
.TP
.B sema_down(struct semaphore *sema)
Decrease the counter in the semaphore, waits if it would become less
than zero. Also known as \fIwait\fR.
.TP
.B struct lock
An implementation of a lock.
.TP
.B lock_init(struct lock *lock)
Initialize a lock.
.TP
.B lock_acquire(struct lock *lock)
Acquire a lock. Might wait.
.TP
.B lock_release(struct lock *lock)
Release a lock. Has to be done by the same thread that called \fIlock_acquire\fR.
.TP
.B struct condition
Implementation of a condition variable.
.TP
.B cond_init(struct condition *cond)
Initialize a condition variable.
.TP
.B cond_wait(struct condition *cond, struct lock *lock)
Cause the current thread to wait until the condition is
signalled. Assumes that the lock is held. The lock will be released
while the thread is waiting, but it will be re-acquired before
\fIcond_wait\fR returns.
.TP
.B cond_signal(struct condition *cond, struct lock *lock)
Wake one thread that is currently waiting inside
\fIcond_wait\fR. Assumes that the lock is held.
.TP
.B cond_broadcast(struct condition *cond, struct lock *lock)
Wake \fIall\fR threads that are currently waiting inside
\fIcond_wait\fR.
.SH ATOMIC OPERATIONS
A number of atomic operations are also supported. Most of these are
generic, meaning that they are overloaded to work for more than one
type. Here, we use \fBP\fR to mean any pointer type, \fBI\fR to mean
any integer type (i.e. signed and unsigned integers, as well as
booleans), and \fBP/I\fR to mean any pointer or integer type.
.TP
.B I test_and_set(I *v)
Read \fIv\fR and return its value, also setting \fIv\fR to 1.
.TP
.B I atomic_add(I *v, I add)
Adds \fIadd\fR to the value pointed to by \fIv\fR. Returns the old value.
.TP
.B I atomic_sub(I *v, I sub)
Subtracts \fIsub\fR from the value pointed to by \fIv\fR. Returns the old value.
.TP
.B P/I atomic_read(P/I *v)
Read from \fIv\fR and return the value.
.TP
.B void atomic_write(P/I *to, P/I v)
Write to \fIto\fR.
.TP
.B P/I atomic_swap(P/I *v, P/I replace)
Read the value pointed to by \fIv\fR, and replace it with the value
\fIreplace\fR. Returns the old value.
.TP
.B P/I compare_and_swap(P/I *v, P/I compare, P/I swap)
Read the value from \fIv\fR, if it was equal to \fIcompare\fR, replace
it with \fIswap\fR. Returns the old value.
.SH SEE ALSO
.BR storm (1)
- the language in which Progvis is implemented.
